package fr.centralesupelec.compenginerestapi;

public class IndividualRegisterGlycemie {

    private final String timestamp;
    private final Integer id_patient;
    private final Double glycemie_level;

    public IndividualRegisterGlycemie(String timestamp,
                              Integer id_patient,
                              Double glycemie_level){
        this.timestamp = timestamp;
        this.id_patient = id_patient;
        this.glycemie_level = glycemie_level;
    }

    public String getTimestamp() {
        return timestamp;
    }
    public Integer getId_patient() {
        return id_patient;
    }
    public Double getGlycemie_level() {
        return glycemie_level;
    }

}
