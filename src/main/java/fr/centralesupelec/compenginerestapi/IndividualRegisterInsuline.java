package fr.centralesupelec.compenginerestapi;

public class IndividualRegisterInsuline {

    private final String timestamp;
    private final Integer id_patient;
    private final Double injection_quantity;

    public IndividualRegisterInsuline(String timestamp,
                                      Integer id_patient,
                                      Double injection_quantity) {
        this.timestamp = timestamp;
        this.id_patient = id_patient;
        this.injection_quantity = injection_quantity;
    }

    public String getTimestamp() {
        return timestamp;
    }
    public Integer getId_patient() {
        return id_patient;
    }
    public Double getInjection_quantity() {
        return injection_quantity;
    }

}
