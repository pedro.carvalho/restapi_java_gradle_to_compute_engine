package fr.centralesupelec.compenginerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompenginerestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompenginerestapiApplication.class, args);
	}

}
