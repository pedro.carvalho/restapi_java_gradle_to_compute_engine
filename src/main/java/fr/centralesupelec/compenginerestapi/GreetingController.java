package fr.centralesupelec.compenginerestapi;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;



@RestController
public class GreetingController {


    public static Connection connect() {

        String url = "jdbc:mysql://104.199.8.8/donnees_application";

        String username = "pedro";
        String password = "00000";
        Connection conn = null;

        System.out.println("Connecting database...");

        try {
            conn = DriverManager.getConnection(url, username, password);
            //conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return conn;
    }

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();


    @GetMapping("/glycemie")
    public String glycemie(@RequestParam(value = "patientID", defaultValue = "fail") String patientID) {

        if (patientID.equals("fail")) {
            return "fail,fail";
        } else {
            String query = "SELECT * FROM Donnees_glycemie WHERE ID_Patient=" + patientID + ";";
            /// MAKE PREPARED STATEMENT!

            try (Connection conn = connect();
                 Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(query)) {

                Vector<IndividualRegisterGlycemie> myVectorGlycemie = new Vector<>();

                while (rs.next()) {
                    IndividualRegisterGlycemie tmpIndividualRegisterGlycemie = new IndividualRegisterGlycemie(
                            rs.getString("Timestamp"),
                            rs.getInt("ID_Patient"),
                            rs.getDouble("Niveau_mesure_glycemie"));

                    myVectorGlycemie.addElement(tmpIndividualRegisterGlycemie);
                }

                String myVectorJSONString = "{";

                for (Integer i = 0; i < myVectorGlycemie.size(); i++) {
                    Integer j = i + 1;
                    myVectorJSONString = myVectorJSONString + "\n\"" + j.toString() + "\" : {\n";
                    myVectorJSONString = myVectorJSONString + "\"timestamp\": \"" + myVectorGlycemie.get(i).getTimestamp() + "\",\n";
                    myVectorJSONString = myVectorJSONString + "\"id_patient\": " + myVectorGlycemie.get(i).getId_patient() + ",\n";
                    myVectorJSONString = myVectorJSONString + "\"sugar_level\": " + myVectorGlycemie.get(i).getGlycemie_level() + "\n";
                    myVectorJSONString = myVectorJSONString + "}";
                    if (i != myVectorGlycemie.size() - 1) {
                        myVectorJSONString = myVectorJSONString + ",";
                    }
                }

                myVectorJSONString = myVectorJSONString + "\n}";
                return myVectorJSONString;

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return "fail,fail";
            }
        }
    }


    @GetMapping("/insuline")
    public String insuline(@RequestParam(value = "patientID", defaultValue = "fail") String patientID) {

        if (patientID.equals("fail")) {
            return "fail,fail";
        } else {
            String query = "SELECT * FROM Donnees_insuline WHERE ID_Patient=" + patientID + ";";
            /// MAKE PREPARED STATEMENT!
            //newcode
            /*String query = "SELECT * FROM Donnees_insuline WHERE ID_Patient=(?);";

            try {
                Connection conn = connect();

                PreparedStatement preparedStmt = conn.prepareStatement(query);

                Integer ID_Patient = 2;
                preparedStmt.setInt(1, ID_Patient);
                preparedStmt.setString(2, timestampString);
                preparedStmt.setDouble(3, insuline);

                boolean successful = preparedStmt.execute();
                if (successful) {
                    System.out.println("Successfully added blood data");
                }*/

            try (Connection conn = connect();
                 Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(query)) {

                Vector<IndividualRegisterInsuline> myVectorInsuline = new Vector<>();

                while (rs.next()) {
                    IndividualRegisterInsuline tmpIndividualRegisterInsuline = new IndividualRegisterInsuline(
                            rs.getString("Timestamp"),
                            rs.getInt("ID_Patient"),
                            rs.getDouble("Quantite_injection_insuline"));

                    myVectorInsuline.addElement(tmpIndividualRegisterInsuline);
                }

                String myVectorJSONString = "{";

                for (Integer i = 0; i < myVectorInsuline.size(); i++) {
                    Integer j = i + 1;
                    myVectorJSONString = myVectorJSONString + "\n\"" + j.toString() + "\" : {\n";
                    myVectorJSONString = myVectorJSONString + "\"timestamp\": \"" + myVectorInsuline.get(i).getTimestamp() + "\",\n";
                    myVectorJSONString = myVectorJSONString + "\"id_patient\": " + myVectorInsuline.get(i).getId_patient() + ",\n";
                    myVectorJSONString = myVectorJSONString + "\"injection_quantity\": " + myVectorInsuline.get(i).getInjection_quantity() + "\n";
                    myVectorJSONString = myVectorJSONString + "}";
                    if (i != myVectorInsuline.size() - 1) {
                        myVectorJSONString = myVectorJSONString + ",";
                    }
                }

                myVectorJSONString = myVectorJSONString + "\n}";
                return myVectorJSONString;

            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return "fail,fail";
            }
        }
    }


    @GetMapping("/envoi-insuline")
    public EnvoiInsuline envoiInsuline(@RequestParam(value = "value", defaultValue = "null") String value) {

        if (value.equals("null")) {
            System.out.println("No value was passed");
        } else {
            System.out.println("About to split");
            String[] arrayOfData = value.split(",");

            Double insuline = Double.parseDouble(arrayOfData[0]);

            Integer time = Integer.parseInt(arrayOfData[1]);
            LocalDateTime timestamp = LocalDateTime.now().plusSeconds(time);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String timestampString = timestamp.format(formatter);

            String query = "INSERT INTO Donnees_insuline(ID_Patient, Timestamp, Quantite_injection_insuline) " +
                    "VALUES (?, ?, ?);";

            try {
                Connection conn = connect();

                PreparedStatement preparedStmt = conn.prepareStatement(query);

                Integer ID_Patient = 3;
                preparedStmt.setInt(1, ID_Patient);
                preparedStmt.setString(2, timestampString);
                preparedStmt.setDouble(3, insuline);

                boolean successful = preparedStmt.execute();
                if (successful) {
                    System.out.println("Successfully added blood data");
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return new EnvoiInsuline(counter.incrementAndGet(), "Tried sending insuline values");
    }


    @GetMapping("/envoi-glycemie")
    public EnvoiGlycemie envoiGlycemie(@RequestParam(value = "value", defaultValue = "null") String value) {

        // value: (measure,timestamp)

        if (value.equals("null")) {
            System.out.println("No value was passed");
        } else {
            System.out.println("About to split");
            String[] arrayOfData = value.split(",");

            Double glycemie = Double.parseDouble(arrayOfData[0]);

            Integer time = Integer.parseInt(arrayOfData[1]);
            LocalDateTime timestamp = LocalDateTime.now().plusSeconds(time);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String timestampString = timestamp.format(formatter);

            String query = "INSERT INTO Donnees_glycemie(ID_Patient, Timestamp, Niveau_mesure_glycemie) " +
                    "VALUES (?, ?, ?);";

            try {
                Connection conn = connect();

                PreparedStatement preparedStmt = conn.prepareStatement(query);

                Integer ID_Patient = 3;
                preparedStmt.setInt(1, ID_Patient);
                preparedStmt.setString(2, timestampString);
                preparedStmt.setDouble(3, glycemie);

                boolean successful = preparedStmt.execute();
                System.out.println("Just executed query");
                if (successful) {
                    System.out.println("Successfully added blood data");
                }

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return new EnvoiGlycemie(counter.incrementAndGet(), "Tried sending glycemie values");
    }


    @GetMapping("/auth")
    public String auth(@RequestParam(value = "logInInfo", defaultValue = "null") String logInInfo) {

        String password = "";
        String categorie = "";
        String email = "";

        if (logInInfo.equals("null")) {
            System.out.println("No authentification!");
        } else {

            String[] arrayOfData = logInInfo.split(",");

            email = arrayOfData[0];
            password = arrayOfData[1];
            categorie = arrayOfData[2];

            //will go through all resultset comparing ; if doesn't find anything, keep like this; if finds, turns to true
            boolean authOK = false;
            Integer ID = null;

            System.out.println("Start Authentification:");

            if (categorie.equals("Medecin")) {
                String query = "SELECT Medecin.ID_Medecin,\n" +
                        "Compte.email,\n" +
                        "Compte.Password\n" +
                        "FROM Compte\n" +
                        "INNER JOIN Medecin\n" +
                        "WHERE Medecin.e_mail_medecin = Compte.email";

                try {
                    Connection conn = connect();
                    PreparedStatement st = conn.prepareStatement(query);
                    ResultSet rs = st.executeQuery();
                    while (rs.next()) {
                        System.out.println("OK1");
                        if (rs.getString("email").equals(email)) {
                            System.out.println("OK2");
                            if (rs.getString("Password").equals(password)) {
                                System.out.println("OK3");
                                authOK = true;
                                ID = rs.getInt("ID_Medecin");
                            }
                        }
                    }
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }

            } else if (categorie.equals("Patient")) {
                String query = "SELECT Patient.ID_Patient,\n" +
                        "Compte.email,\n" +
                        "Compte.Password\n" +
                        "FROM Compte\n" +
                        "INNER JOIN Patient\n" +
                        "WHERE Patient.e_mail_patient = Compte.email";

                try {
                    Connection conn = connect();
                    PreparedStatement st = conn.prepareStatement(query);
                    ResultSet rs = st.executeQuery();
                    while (rs.next()) {
                        System.out.println("OK1");
                        if (rs.getString("email").equals(email)) {
                            System.out.println("OK2");
                            if (rs.getString("Password").equals(password)) {
                                System.out.println("OK3");
                                authOK = true;
                                ID = rs.getInt("ID_Patient");
                            }
                        }
                    }
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                }

            } else {
                return "fail,fail";
            }

            if (authOK) {
                return ID.toString() + "," + categorie;
            }

        }
        return "fail,fail";
    }


    @GetMapping("/list_patients")
    public String ListPatients(@RequestParam(value = "ID_Medecin", defaultValue = "null") String ID_Medecin) {

        if (ID_Medecin.equals("null")) {
            return "fail";
        } else {

            //a list of all the patients of a medecin

            String query = "SELECT ID_Patient FROM Patient WHERE ID_Medecin=" + ID_Medecin;

            try (Connection conn = connect();
                 Statement stmt = conn.createStatement();
                 ResultSet rs = stmt.executeQuery(query)) {

                Vector<String> patientsVector = new Vector<>();

                while (rs.next()) {
                    patientsVector.addElement(Integer.toString(rs.getInt("ID_Patient")));
                }

                String patientsString = "";
                for (int j = 0; j < patientsVector.size(); j++) {
                    patientsString = patientsString + patientsVector.get(j);
                    if (j < patientsVector.size() - 1) {
                        patientsString = patientsString + ",";
                    }
                }

                return patientsString;

            } catch (SQLException throwables) {
                throwables.printStackTrace();
                return "fail";
            }
        }
    }
}



