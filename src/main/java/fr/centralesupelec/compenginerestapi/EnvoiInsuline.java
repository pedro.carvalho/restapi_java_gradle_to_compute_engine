package fr.centralesupelec.compenginerestapi;

public class EnvoiInsuline {

    private final long id;
    private final String content;

    public EnvoiInsuline(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

}
