package fr.centralesupelec.compenginerestapi;

public class EnvoiGlycemie {
    private final long id;
    private final String content;

    public EnvoiGlycemie(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
}
